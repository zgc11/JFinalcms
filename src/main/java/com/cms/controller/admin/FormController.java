package com.cms.controller.admin;

import java.util.Date;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;

import com.cms.Feedback;
import com.cms.entity.Form;
import com.cms.routes.RouteMapping;
import com.cms.util.TableUtils;


/**
 * Controller - 表单
 * 
 * 
 * 
 */
@RouteMapping(url = "/admin/form")
public class FormController extends BaseController {
	
	/**
     * 列表
     */
    public void index(){
    	setListQuery();
    	Integer pageNumber = getParaToInt("pageNumber");
		if(pageNumber==null){
			pageNumber = 1;
		}
    	setAttr("page", new Form().dao().findPage(pageNumber,PAGE_SIZE));
        render(getView("form/index"));
    }
    
	/**
	 * 添加
	 */
	public void add() {
		render(getView("form/add"));
	}
	
	/**
	 * 保存
	 */
	public void save() {
		Form form =  getModel(Form.class,"",true); 
		form.setCreateDate(new Date());
		form.setUpdateDate(new Date());
		form.save();
		//创建数据库表
		TableUtils.create(form.getTableName());
		redirect(getListQuery("/admin/form"));
	}

	/**
	 * 编辑
	 */
	public void edit() {
		Integer id = getParaToInt("id");
		Form form = new Form().dao().findById(id);
		setAttr("form", form);
		render(getView("form/edit"));
	}

	/**
	 * 修改
	 */
	public void update() {
		Form form =  getModel(Form.class,"",true); 
		form.setUpdateDate(new Date());
		form.update();
		redirect(getListQuery("/admin/form"));
	}

	/**
	 * 删除
	 */
	public void delete() {
		Integer ids[] = getParaValuesToInt("ids");
		if(ArrayUtils.isNotEmpty(ids)){
			for(Integer id:ids){
				Form form = new Form().dao().findById(id);
				TableUtils.delete(form.getTableName());
				form.delete();
			}
		}
		renderJson(Feedback.success(new HashMap<>()));
	}

}